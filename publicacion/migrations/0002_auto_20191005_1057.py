# Generated by Django 2.2.5 on 2019-10-05 13:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publicacion', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='propietario',
            name='dv',
            field=models.CharField(default='', max_length=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='propietario',
            name='fijo',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='propietario',
            name='movil',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='propietario',
            name='rut',
            field=models.IntegerField(default=1, unique=True),
            preserve_default=False,
        ),
    ]
